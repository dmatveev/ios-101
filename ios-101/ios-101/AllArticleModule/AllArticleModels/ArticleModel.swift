//
//  ArticleModel.swift
//  ios-101
//
//  Created by Denis Matveev on 19.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import Foundation
import ObjectMapper

class ArticleModel: NSObject, Mappable {
    var id: Int?
    var userId: Int?
    var title: String?
    var body: String?
    var commentCount: Int?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        id      <- map["id"]
        userId  <- map["userId"]
        title   <- map["title"]
        body    <- map["body"]
    }

}
