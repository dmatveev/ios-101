//
//  SharedApiService.swift
//  ios-101
//
//  Created by Denis Matveev on 19.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import Foundation

class SharedApiService: NSObject {
    static let sharedInstance: SharedApiService = { SharedApiService() }()
    
    private(set) var articleService: ArticleService
    private(set) var commentService: CommentService
    private(set) var articleUserService: ArticleUserService
    
    private override init() {
        self.articleService = ArticleService()
        self.commentService = CommentService()
        self.articleUserService = ArticleUserService()
    }
    
}
