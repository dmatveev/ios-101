//
//  ArticleService.swift
//  ios-101
//
//  Created by Denis Matveev on 19.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyJSON

typealias ArticlesPostCompletion = (_ articles: [ArticleModel]?, _ error: String?) -> Void

class ArticleService: TypicodeApiService {
    
    func post(limit: Int, skip: Int, completion: @escaping ArticlesPostCompletion) {
        let url = host + "/posts"
        
        var params:[String: AnyObject] = [:]
        params["limit"] = limit as AnyObject
        params["skip"] = limit as AnyObject
        
        self.sendRequestWithJSONResponce (
            requetType: HTTPMethod.get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                if error != nil {
                    completion(nil, error!.localizedDescription)
                    return
                } else if let articles = responseData!.arrayObject {
                    let articlesModels = Mapper<ArticleModel>().mapArray(JSONObject: articles)
                    
                    completion(articlesModels, nil)
                    return
                }
                
                completion(nil, "Ошбка загрузки каталога")
        }
    }
    
}
