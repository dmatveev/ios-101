//
//  AllArticlesViewController.swift
//  ios-101
//
//  Created by Denis Matveev on 14.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import UIKit

class AllArticlesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AllArticlesDataProviderDelegate {
    
    
    // MARK: PARAMS
    
    static let nibName = "AllArticlesViewController"
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataProvider: AllArticlesDataProvider = AllArticlesDataProvider()
    
    // MARK: CONTROLLER
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataProvider.delegate = self
        
        costomizeTableView()
        
        dataProvider.loadArticles()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: TABLE VIEW
    
    func costomizeTableView () {
        tableView.register(UINib(nibName: ArticleTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleTableViewCell.nibName)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProvider.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellInStoryboard")
           // cell?.isUserInteractionEnabled = false
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ArticleTableViewCell.nibName) as! ArticleTableViewCell
            cell.customize(article: dataProvider.articles[indexPath.row])
            cell.clipsToBounds = false
            cell.layer.masksToBounds = false
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOpacity = 0.2
            cell.layer.opacity = 0.3
            cell.layer.shadowOffset = CGSize(width: 0, height: 4)
            cell.layer.shadowRadius = 4
            cell.layer.cornerRadius = 4
            cell.layer.zPosition = 1
            //cell.isUserInteractionEnabled = true
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row % 2 == 0 {
            return 20
        } else {
            return 250
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row % 2 != 0 {
        ArticleRouting.showAtricle(article: dataProvider.articles[indexPath.row], fromVc: self)
        }
    }
    
    // MARK: - AllArticlesDataProviderDelegate
    
    func articlesDataLoader() {
        tableView.reloadData()
    }
    
    func articlesDataHasError(error: String) {
        
    }
    
    
    
    
}
