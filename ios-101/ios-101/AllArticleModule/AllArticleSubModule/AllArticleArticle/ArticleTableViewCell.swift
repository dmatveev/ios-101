//
//  ArticleTableViewCell.swift
//  ios-101
//
//  Created by Denis Matveev on 15.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
    
    
    
    static let nibName = "ArticleTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var commentsCountLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    func customize (article: ArticleModel) {
        titleLabel.text = article.title
        bodyLabel.text = article.body
        if article.commentCount != nil {
            commentsCountLabel.text = String(describing: article.commentCount!)
            commentsCountLabel.isHidden = false
            activityIndicator.isHidden = true
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
