//
//  AllArticlesDataProvider.swift
//  ios-101
//
//  Created by Denis Matveev on 19.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import Foundation

protocol AllArticlesDataProviderDelegate: class {
    func articlesDataLoader()
    func articlesDataHasError(error: String)
}

class AllArticlesDataProvider: NSObject {
    
    var articles:[ArticleModel] = []
    var comments:[CommentModel] = []
    var commentsCount: Int?
    weak var delegate: AllArticlesDataProviderDelegate?
    
    func refresh(){
        articles = []
        loadArticles()
    }
    
    func loadArticles () {
        SharedApiService.sharedInstance.articleService.post(limit: 100, skip: 0){ (articlesResponce, errors) in
            
            if let error = errors {
                self.delegate?.articlesDataHasError(error: error)
                return
            } else if let articles:[ArticleModel] = articlesResponce {
                self.articles.append(contentsOf: articles)
                for index in 0...articles.count - 1 {
                    SharedApiService.sharedInstance.commentService.comments(postId: articles[index].id!, completion: { (commentsResponse, errors) in
                        
                        if let error = errors {
                            self.delegate?.articlesDataHasError(error: error)
                            return
                        } else if let commentsArray:[CommentModel] = commentsResponse {
                            self.articles[index].commentCount = commentsArray.count
                            self.delegate?.articlesDataLoader()
                            return
                        }
                        
                        self.delegate?.articlesDataLoader()
                        
                    })
                }
                self.delegate?.articlesDataLoader()
                return
            }
            
            self.delegate?.articlesDataLoader()
            
        }
        
    }
    
    
}
