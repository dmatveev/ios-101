//
//  UserModel.swift
//  ios-101
//
//  Created by Denis Matveev on 21.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import Foundation
import ObjectMapper

class UserModel: NSObject, Mappable {
    var id: Int?
    var img: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        img         <- map["img"]

    }
    
}
