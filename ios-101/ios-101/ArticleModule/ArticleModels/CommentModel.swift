//
//  CommentModel.swift
//  ios-101
//
//  Created by Denis Matveev on 19.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import Foundation
import ObjectMapper

class CommentModel: NSObject, Mappable {
    var postId: Int?
    var id: Int?
    var name: String?
    var email: String?
    var body: String?
    var imgURL: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        postId      <- map["postId"]
        id          <- map["id"]
        name        <- map["name"]
        email       <- map["email"]
        body        <- map["body"]
        
    }
    
}
