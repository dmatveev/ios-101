//
//  ArticleDataProvider.swift
//  ios-101
//
//  Created by Denis Matveev on 19.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import Foundation

protocol ArticleDataProviderDelegate: class {
    func articleDataLoader()
    func articleDataHasError(error: String)
}

class ArticleDataProvider: NSObject {
    
    var article:ArticleModel = ArticleModel()
    var comments:[CommentModel] = []
    var users:[UserModel] = []
    weak var delegate: ArticleDataProviderDelegate?
    
    func refresh(){
        comments = []
        users = []
        loadData()
    }
    
    func loadData () {
        if article.id != nil {
            SharedApiService.sharedInstance.commentService.comments(postId: article.id!, completion: { (commentsResponse, errors) in
                if let error = errors {
                    self.delegate?.articleDataHasError(error: error)
                    return
                } else if let commentsArray:[CommentModel] = commentsResponse {
                    self.comments.append(contentsOf: commentsArray)
                    for index in 0...commentsArray.count - 1  {
                        SharedApiService.sharedInstance.articleUserService.users(id: commentsArray[index].id!, completion: { (usersResponse, errors) in
                            if let error = errors {
                                self.delegate?.articleDataHasError(error: error)
                                return
                            } else if let usersArray:[UserModel] = usersResponse {
                                self.comments[index].imgURL = "https://api.opendota.com" + usersArray[index].img!
                                self.delegate?.articleDataLoader()
                                return
                            }
                            self.delegate?.articleDataLoader()
                        })
                    }
                    self.delegate?.articleDataLoader()
                    return
                }
                self.delegate?.articleDataLoader()
            })
        }
    }
    
    
}








