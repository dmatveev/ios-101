//
//  CommentService.swift
//  ios-101
//
//  Created by Denis Matveev on 19.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyJSON

typealias CommentsCompletion = (_ comments: [CommentModel]?, _ error: String?) -> Void

class CommentService: TypicodeApiService {
    
    func comments(postId: Int, completion: @escaping CommentsCompletion) {
        let url = host + "/comments"
        
        var params:[String : AnyObject] = [:]
        params["postId"] = postId as AnyObject
        
        self.sendRequestWithJSONResponce (
            requetType: HTTPMethod.get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                if error != nil {
                    completion(nil, error!.localizedDescription)
                    return
                } else if let comments = responseData!.arrayObject {
                    let commentsModels = Mapper<CommentModel>().mapArray(JSONObject: comments)
                    
                    completion(commentsModels, nil)
                    return
                }
                
                completion(nil, "Ошбка загрузки каталога")
        }
    }
    
}
