//
//  ArticleUserService.swift
//  ios-101
//
//  Created by Denis Matveev on 21.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyJSON

typealias ArticlesUserPostCompletion = (_ users: [UserModel]?, _ error: String?) -> Void

class ArticleUserService: TypicodeApiService {
    
    func users(id: Int?, completion: @escaping ArticlesUserPostCompletion) {
        let url = userHost + "/api/heroStats"
        
        var params:[String: AnyObject] = [:]
        params["id"] = id as AnyObject
        
        self.sendRequestWithJSONResponce (
            requetType: HTTPMethod.get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                if error != nil {
                    completion(nil, error!.localizedDescription)
                    return
                } else if let users = responseData!.arrayObject {
                    let userModels = Mapper<UserModel>().mapArray(JSONObject: users)
                    
                    completion(userModels, nil)
                    return
                }
                
                completion(nil, "Ошбка загрузки каталога")
        }
    }
    
}
