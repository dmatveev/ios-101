//
//  ArticleContentTableViewCell.swift
//  ios-101
//
//  Created by Denis Matveev on 15.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import UIKit

class ArticleContentTableViewCell: UITableViewCell {

    static let nibName = "ArticleContentTableViewCell"
    
    @IBOutlet weak var bodyLabel: UILabel!
    
    func customize (article: ArticleModel) {
        bodyLabel.text = article.body
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
