//
//  CommentTableViewCell.swift
//  ios-101
//
//  Created by Denis Matveev on 15.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import UIKit
import Kingfisher

class CommentTableViewCell: UITableViewCell {
    
    static let nibName = "CommentTableViewCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    
    func customize (comment: CommentModel) {
        nameLabel.text = comment.name
        commentLabel.text = comment.body
        
       if comment.imgURL != nil{
            let imageURL = URL(string: comment.imgURL!)
            let resource = ImageResource(downloadURL: imageURL!, cacheKey: String(describing: comment.id))
            self.userImg.kf.setImage(with: resource)
            self.userImg.layer.cornerRadius = self.userImg.frame.size.width/2
            self.userImg.clipsToBounds = true
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
