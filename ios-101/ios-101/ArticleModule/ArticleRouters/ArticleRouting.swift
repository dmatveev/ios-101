//
//  ArticleRouting.swift
//  ios-101
//
//  Created by Denis Matveev on 20.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import Foundation
import UIKit

class ArticleRouting {
    static func showAtricle (article: ArticleModel, fromVc: UIViewController) {
        let articleVc = UIStoryboard(name: "Article", bundle: nil).instantiateViewController(withIdentifier: ArticlesViewController.nibName) as! ArticlesViewController
        
        articleVc.dataProvider.article = article
        
        fromVc.navigationController?.pushViewController(articleVc, animated: true)
    }
}
