//
//  ArticlesViewController.swift
//  ios-101
//
//  Created by Denis Matveev on 15.06.2018.
//  Copyright © 2018 Denis Matveev. All rights reserved.
//

import UIKit

class ArticlesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ArticleDataProviderDelegate {
    
    
    
    // MARK: PARAMS
    
    static let nibName = "ArticlesViewController"
    
    @IBOutlet weak var tableView: UITableView!
    
    public var dataProvider: ArticleDataProvider = ArticleDataProvider()
    
    // MARK: CONTROLLER
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataProvider.delegate = self
        
        costomizeTable()
        
        dataProvider.loadData()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    // MARK: TABLE VIEW
    
    func costomizeTable (){
        
        tableView.register(UINib(nibName: ArticleTitleTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleTitleTableViewCell.nibName)
        
        tableView.register(UINib(nibName: ArticleContentTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleContentTableViewCell.nibName)
        
        tableView.register(UINib(nibName: CommentTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: CommentTableViewCell.nibName)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        } else {
            return dataProvider.comments.count
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ArticleTitleTableViewCell.nibName) as! ArticleTitleTableViewCell
            cell.customize(article: dataProvider.article)
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ArticleContentTableViewCell.nibName) as! ArticleContentTableViewCell
            cell.customize(article: dataProvider.article)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.nibName) as! CommentTableViewCell
            cell.customize(comment: dataProvider.comments[indexPath.row])
            cell.separatorInset = UIEdgeInsets.init(top: 0, left: 19, bottom: 0, right: 0)
            return cell
        }
        
    }
    
    // MARK: - AllArticlesDataProviderDelegate
    
    func articleDataLoader() {
        tableView.reloadData()
    }
    
    func articleDataHasError(error: String) {
        
    }
    
    
    
    
}
